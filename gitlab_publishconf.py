#!/usr/bin/env python
# -*- coding: utf-8 -*- #

import os
import sys
sys.path.append(os.curdir)
from publishconf import *

SITEURL = 'https://faizov_boris.gitlab.io/new-gml-site'

MENUITEMS_LANG = [
    ('ru', [('Главная', SITEURL + '/home-ru.html'),
            ('О лаборатории', SITEURL + '/pages/about-lab-ru.html'),
            ('Сотрудники', SITEURL + '/researchers-ru.html'),
            ('Проекты', SITEURL + '/projects-ru.html'),
            ('Публикации', SITEURL + '/publications-ru.html'),
            ('Загрузки', SITEURL + '/downloads-ru.html'),
            ('Преподавание', SITEURL + '/teaching-ru.html'),
            ('Выпускники', SITEURL + '/alumni-ru.html')]),
    ('en', [('Home', SITEURL + '/home.html'),
            ('About Lab', SITEURL + '/pages/about-lab.html'),
            ('People', SITEURL + '/researchers.html'),
            ('Research', SITEURL + '/projects.html'),
            ('Publications', SITEURL + '/publications.html'),
            ('Downloads', SITEURL + '/downloads.html'),
            ('Teaching', SITEURL + '/teaching.html'),
            ('Alumni', SITEURL + '/alumni.html')]),
]

# -*- coding: utf-8 -*-

import logging
import os
import os.path
import copy
from collections import defaultdict
from functools import partial
from operator import attrgetter

from pelican import signals
from pelican.contents import Content
from pelican.generators import ArticlesGenerator


def FindTranslation(obj, lang):
    for lang_obj in [obj] + obj.translations:
        if lang_obj.lang == lang:
            return lang_obj
    return None

def GetLangPageUrl(settings, slug, lang=None):
    if lang == settings['DEFAULT_LANG'] or lang is None:
        url = '{slug}.html'.format(slug=slug)
    else:
        url = '{slug}-{lang}.html'.format(slug=slug, lang=lang)
    return url

def GetAllTranslationsUrls(settings, slug, cur_lang=None):
    translations = []
    for tr_lang in settings['POSSIBLE_LANGS']:
        if tr_lang == cur_lang:
            continue
        translations.append({
            'lang': tr_lang,
            'url': GetLangPageUrl(settings, slug, tr_lang)
        })
    return translations

def GetExistingTranslationsUrls(obj, url_field, cur_lang=None):
    translations = []
    for translation in obj.translations:
        translations.append({
            'lang': translation.lang,
            'url': getattr(translation, url_field)
        })
    return translations

def GetListOfAllTranslations(objs):
    return sum([obj.translations for obj in objs], objs)


class Holder(object):
    # First element always should be 'id'
    info_fields = ['id']
    category_name = ''

    def __init__(self, categories, settings):
        self.settings = settings
        articles = sum([v for k, v in categories if k == self.category_name], [])
        self.objects = []
        self.id_to_objects_pos = {}
        for article in articles:
            self.id_to_objects_pos[article.id] = len(self.objects)
            self.objects.append(article)
    
    def get_info_by_ids(self, need_ids, need_lang=None):
        return_infos = []
        for cur_id in need_ids:
            cur_object = None
            if cur_id in self.id_to_objects_pos.keys():
                cur_object = self.objects[self.id_to_objects_pos[cur_id]]
                if need_lang is not None:
                    cur_object = FindTranslation(cur_object, need_lang)
            if cur_object is None:
                return_infos.append({
                    field_name: 'None' for field_name in self.info_fields
                })
                return_infos[-1]['id'] = cur_id
            else:
                return_infos.append({
                    field_name: getattr(cur_object, field_name) for field_name in self.info_fields
                })
        return return_infos


    def get_objects_by_ids(self, need_ids, need_lang=None):
        return_objects = []
        for cur_id in need_ids:
            cur_object = None
            if cur_id in self.id_to_objects_pos.keys():
                cur_object = self.objects[self.id_to_objects_pos[cur_id]]
                if need_lang is not None:
                    cur_object = FindTranslation(cur_object, need_lang)
            if cur_object is None:
                return_objects.append([cur_id, 'None'])
            else:
                return_objects.append([cur_id, cur_object])
        return return_objects


class PeopleHolder(Holder):
    info_fields = ['id', 'name', 'person_url']
    category_name = 'people'

    def __init__(self, categories, settings):
        '''
        Доступные поля в objects:
        [persontype, id, title, name, email, photo, position, researchinterests,
         projects, selectedpublications, about, url, ]
        '''
        super().__init__(categories, settings)
        for person in GetListOfAllTranslations(self.objects):
            person.about = person.content
            person.name = person.title
            person.researchinterests = list(person.researchinterests.strip().split(', '))
            person.projects = list(person.projects.strip().split(', '))
            person.selectedpublications = list(person.selectedpublications.strip().split(', '))
            person.person_url = 'people/' + GetLangPageUrl(self.settings, person.id, person.lang)


class ProjectsHolder(Holder):
    info_fields = ['id', 'title', 'smallphoto', 'project_url']
    category_name = 'projects'

    def __init__(self, categories, settings):
        '''
        Доступные поля в objects:
        [projecttype, id, smallphoto, photo,
         title, contactperson, team, selectedpublications, content]
        '''
        super().__init__(categories, settings)
        for project in GetListOfAllTranslations(self.objects):
            project.selectedpublications = list(project.selectedpublications.strip().split(', '))
            project.team = list(project.team.strip().split(', '))
            project.project_url = 'projects/' + GetLangPageUrl(self.settings, project.id, project.lang)


class PublicationsHolder(Holder):
    info_fields = ['id', 'title', 'paperauthors', 'journal', 'downloadlink']
    category_name = 'publications'

    def __init__(self, categories, settings):
        '''
        Доступные поля в objects:
        [id, photo, title, paperauthors, journal, downloadlink, content]
        '''
        super().__init__(categories, settings)
        for publication in GetListOfAllTranslations(self.objects):
            for i, line in enumerate(publication.bibtex[:-1]):
                publication.bibtex[i] = line + "<br>"
            publication.bibtex = ''.join(publication.bibtex)
            publication.paperauthors = list(publication.paperauthors.strip().split(', '))
            publication.translations = []
            for lang in self.settings['POSSIBLE_LANGS']:
                if lang == self.settings['DEFAULT_LANG']:
                    continue
                lang_publication = copy.copy(publication)
                lang_publication.lang = lang
                publication.translations.append(lang_publication)


class NewsHolder(Holder):
    info_fields = ['id']
    category_name = 'news'

    def __init__(self, categories, settings):
        '''
        Доступные поля в objects:
        [title, id, people_names, date,
         summary, summaryImage, content]
        '''
        super().__init__(categories, settings)
        for cur_news in GetListOfAllTranslations(self.objects):
            cur_news.people_names = [author.slug for author in cur_news.authors]


class DataHolder(object):
    def __init__(self, categories, settings):
        self.projects_holder = ProjectsHolder(categories, settings)
        self.people_holder = PeopleHolder(categories, settings)
        self.publications_holder = PublicationsHolder(categories, settings)
        self.news_holder = NewsHolder(categories, settings)

        for publication_article in GetListOfAllTranslations(self.publications_holder.objects):
            publication_article.paperauthors = self.people_holder.get_info_by_ids(publication_article.paperauthors, need_lang=publication_article.lang)

        for news_article in GetListOfAllTranslations(self.news_holder.objects):
            news_article.people_names = self.people_holder.get_info_by_ids(news_article.people_names, need_lang=news_article.lang)

        for person_article in GetListOfAllTranslations(self.people_holder.objects):
            person_article.projects = self.projects_holder.get_info_by_ids(person_article.projects, need_lang=person_article.lang)
            person_article.selectedpublications = self.publications_holder.get_objects_by_ids(person_article.selectedpublications, need_lang=person_article.lang)

        for project_article in GetListOfAllTranslations(self.projects_holder.objects):
            project_article.contactperson = self.people_holder.get_info_by_ids([project_article.contactperson], need_lang=project_article.lang)[0]
            project_article.team = self.people_holder.get_info_by_ids(project_article.team, need_lang=project_article.lang)
            project_article.selectedpublications = self.publications_holder.get_objects_by_ids(project_article.selectedpublications, need_lang=project_article.lang)

class MyGenerator(ArticlesGenerator):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def generate_output(self, writer):
        # print([(elem.url, [tr.url for tr in elem.translations]) for elem in self.articles])
        data = DataHolder(self.categories, self.settings)
        write = partial(writer.write_file, relative_urls=self.settings['RELATIVE_URLS'], context=self.context)

        self.generate_homepage(write, data)
        self.generate_news(write, data)
        self.generate_people_page(write, data)
        self.generate_persons(write, data)
        self.generate_projects_page(write, data)
        self.generate_projects(write, data)
        self.generate_publications_page(write, data)
        signals.article_writer_finalized.send(self, writer=writer)


    def generate_homepage(self, write, data):
        news_articles = sum([v for k, v in self.categories if k == 'news'], [])

        for lang in self.settings['POSSIBLE_LANGS']:
            url = save_as = GetLangPageUrl(self.settings, 'home', lang)
            translations = GetAllTranslationsUrls(self.settings, 'home', lang)
            all_news_link=GetLangPageUrl(self.settings, 'newslist', lang)
            
            current_news = [FindTranslation(news, lang) for news in data.news_holder.objects if FindTranslation(news, lang) is not None]
            current_news.sort(key=attrgetter('date'), reverse=True)
            current_news = current_news[:3]

            write(save_as, self.get_template('home'), template_name='home',
                articles=current_news, lang=lang, translations=translations, all_news_link=all_news_link,
                page_name=os.path.splitext(save_as)[0], url=url)


    def generate_news(self, write, data):
        news_articles = sum([v for k, v in self.categories if k == 'news'], [])

        for lang in self.settings['POSSIBLE_LANGS']:
            url = save_as = GetLangPageUrl(self.settings, 'newslist', lang)
            translations = GetAllTranslationsUrls(self.settings, 'newslist', lang)
            
            current_news = [FindTranslation(news, lang) for news in data.news_holder.objects if FindTranslation(news, lang) is not None]
            write(save_as, self.get_template('news'), template_name='news',
                articles=current_news, lang=lang, translations=translations,
                page_name=os.path.splitext(save_as)[0], url=url)


    def generate_people_page(self, write, data):
        PERSON_PAGES_NAMES = ['researchers', 'students']
        PERSON_TYPES = ['researcher', 'student']
        
        for page_name, person_type in zip(PERSON_PAGES_NAMES, PERSON_TYPES):
            for lang in self.settings['POSSIBLE_LANGS']:
                url = save_as = GetLangPageUrl(self.settings, page_name, lang)
                translations = GetAllTranslationsUrls(self.settings, page_name, lang)

                current_people = [person for person in data.people_holder.objects if person.persontype == person_type]
                current_people = [FindTranslation(person, lang) for person in current_people if FindTranslation(person, lang) is not None]
                write(save_as, self.get_template('people_page'), template_name='people_page',
                    people=current_people, lang=lang, translations=translations,
                    page_name=os.path.splitext(save_as)[0], url=url)


    def generate_persons(self, write, data):
        for person_article in GetListOfAllTranslations(data.people_holder.objects):
            save_as = person_article.person_url
            url = person_article.person_url
            write(save_as, self.get_template('person_page'), template_name='person_page',
                  person_info=person_article, lang=person_article.lang, translations=GetExistingTranslationsUrls(person_article, 'person_url'),
                  page_name=os.path.splitext(save_as)[0], url=url)


    def generate_projects_page(self, write, data):
        for lang in self.settings['POSSIBLE_LANGS']:
            url = save_as = GetLangPageUrl(self.settings, 'projects', lang)
            translations = GetAllTranslationsUrls(self.settings, 'projects', lang)
            current_projects = [FindTranslation(project, lang) for project in data.projects_holder.objects if FindTranslation(project, lang) is not None]

            write(save_as, self.get_template('projects_page'), template_name='projects_page',
                projects=current_projects, lang=lang, translations=translations,
                page_name=os.path.splitext(save_as)[0], url=url)


    def generate_projects(self, write, data):
        for project_article in GetListOfAllTranslations(data.projects_holder.objects):
            save_as = project_article.project_url
            url = project_article.project_url
            write(save_as, self.get_template('project_page'), template_name='project_page',
                  project_info=project_article, lang=project_article.lang, translations=GetExistingTranslationsUrls(project_article, 'project_url'),
                  page_name=os.path.splitext(save_as)[0], url=url)


    def generate_publications_page(self, write, data):
        for lang in self.settings['POSSIBLE_LANGS']:
            url = save_as = GetLangPageUrl(self.settings, 'publications', lang)
            translations = GetAllTranslationsUrls(self.settings, 'publications', lang)

            publications_list = [FindTranslation(publication, lang) for publication in data.publications_holder.objects if FindTranslation(publication, lang) is not None]
            publications_list = [[publication.id, publication] for publication in publications_list]
            write(save_as, self.get_template('publications_page'), template_name='publications_page',
                publications=publications_list, lang=lang, translations=translations,
                page_name=os.path.splitext(save_as)[0], url=url)


def create_generator(pelican_object):
    return MyGenerator


def fix_default_articles(article_generator):
    if isinstance(article_generator, MyGenerator):
        return
    # Чтобы добавить новые поля (и изменить существующие) к новостям нужно сконструировать DataHolder
    DataHolder(article_generator.categories, article_generator.settings)
    # Чтобы оставить только новости (так как дефолтным генератором делаются только они)
    article_generator.articles = [article for article in article_generator.articles if article.category=='news']
    article_generator.translations = [article for article in article_generator.translations if article.category=='news']

def register():
    signals.get_generators.connect(create_generator)
    signals.article_generator_finalized.connect(fix_default_articles)

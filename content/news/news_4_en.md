Title: CMC students won “Evrika” competition in the IT nomination
Category: news
Authors: dmitriy_vatolin, anton_konushin
Date: 2018-04-01 10:00
Id: news_number_4
Lang: en
Summary: CMC students won “Evrika” competition in the IT nomination
SummaryImage: images/evrika_winners.png


A CMC students team won “Evrika” competition in the IT nomination. The final of the competition was held on 19th December, totally 25 teams took place in the competition.

The team members:

Zvezdakov Sergei (master student)
Antsiverova Anastasiya (master student)
Kondranin Denis (bachelor student)
News on the competition website http://kpi-eureka.ru/.

Congrat Sergei, Anastasiya and Denis!


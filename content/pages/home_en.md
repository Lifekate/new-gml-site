Title: Home
Date: 2010-12-03 10:20
Category: home
Id: home
Lang: en

# Home

Graphics and Media Lab (GML) was established in 2002 as a part of Department of Computational Mathematics and Cybernetics of M.V. Lomonosov Moscow State University. Laboratory possesses solid research experience in many areas of computer graphics, computer vision, image and video processing. Research is supported by Russian fund for basic research, state contracts, and by international companies Intel, Microsoft, Samsung.


Title: About lab
Date: 2010-12-03 10:20
Category: about
Id: about
Lang: en

#About lab

Graphics & Media Lab was established as part of department of computational mathematics and cybernetics of M.V. Lomonosov Moscow State University in 1998 and officially confirmed by dean order in 2002. The founder of laboratory was Prof. Yuri. M. Bayakovski, one of the pioneers of computer graphics in Russia and member of ACM Siggraph Computer Graphics Pioneers Club.

The main goals of laboratory are education and research in computer graphics, computer vision, image and video processing. Graphics & Media Lab is an active participant and one of the main organizers of annual interational conference GraphiCon (www.graphicon.ru), which is a leading conference on computer graphics and vision in Russia.

The head of laboratory is Dr. Anton S. Konushin.

Education in computer graphics in Lomonosov Moscow State University started in 1983. Since 1994 the course “Computer graphics” is mandatory for all students of department of computational mathematics and cybernetics. Currently members of our lab also teach several courses: “Introduction in computer vision”, “Topics in computer vision”, “Photorealistic image synthesis”, “Video processing and compression”, “Introduction to medical image analysis”.

Research is currently supported by Intel, Microsoft Research, grants by Russian fund for basic research (RFBR), grants of the President of Russian Federation, state contracts, etc.

# Chronology

1983. The elective course “Machine graphics” was first taught by Dr. Yury M. Bayakovsky in MSU.
1990. The course “Computer graphics” became mandatory for students of 3rd stream (“programming”) of department of computational mathematics and cybernetics.
1994. The course “Computer graphics” became mandatory for all students of department of computational mathematics and cybernetics.
1998. First contracts with Intel. Virtual computer graphics laboratory was established. First room #77 was granted for new laboratory.
1999. Grant from Intel for “Educational and research laboratory on computer graphics and vision” for furnishing and equipping of our second room #703.
2000. Started collaboration with Samsung.
2002. Official opening of Graphics & Media Lab. Official laboratory logo was designed by Yan Sizov. Grant from Samsung for necessary facilities of third room #701.
2003. SAIT-MSU Joint Lab was established. Forth (#76) and fifth (#701a) rooms were added to the lab. New elective course “Video processing and compression” was presented by Dr. Dmitry S. Vatolin.
2005. First time member of the lab (Dr. Vladimir P. Vezhnevets) won grant of President of Russian Federation. Lab website won award from competition “IT-education in Runet” as “Virtual educational complex”.
2006. New elective course “Introduction to computer vision” (Lecturers Dr. Vladimir P. Vezhnevets, Alexander P. Vezhnevets, Dr. Anton S. Konushin).
2007. New elective course “Basics of photorealistic visualization” (Lecturer Dr. Alexey V. Ignatenko). Student of our lab won UMNIK grants for the first time.
2008. Master program “Mathematical and computer methods for image processing” was developed. SAIT-MSU Joint Lab was closed.
2009. With a little delay, in 2009, our Lab celebrated 10 years’ anniversary! The event was signified by two films, available at this page.
2010. First joint research project with Microsoft Research. New elective course “Topics in computer vision” (Lecturer Dr. Anton S. Konushin).
2011. Microsoft Computer Vision Summer School 2011 was held in MSU. It was the first summer school of this calibre in Russia, which was fully dedicated to computer vision. Instead of room #701a laboratory received a new room #702.
2012. New elective course “Modern methods of interactive computer graphics” (Lecturers – Dr. Alexey V. Ignatenko and Vladimir Frolov).
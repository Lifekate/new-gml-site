Category: projects
ProjectType: ongoing
Id: traffic_sign_recognition
Date: 2020-01-01
SmallPhoto: images/traffic_sign_recognition_small.jpg
Photo: images/traffic_sign_recognition_big.jpg
Title: Traffic sign recognition
ContactPerson: anton_konushin
Team: anton_konushin, vlad_shakhuro, dmitriy_vatolin
Lang: en
SelectedPublications: publ1, eval_of_traffic_sign_rec, publ2


Despite many years of active research traffic sign detection and recognition is still an open problem. Recognition accuracy and false detections rate are still not sufficient for replacing human operators with automatic recognition system. We are working on several topics in this area:

- high-speed detection
- sign recognition with multi-layer neural networks
- synthetic datasets generation for training


## Acknowledgements

This project is partially supported by grant RFBR 12-01-33085 «Learning probabilistic models for image recognition, scanning data processing, computer vision» and by Skolkovo Institute of Science and Technology, contract №081-R, Annex A1




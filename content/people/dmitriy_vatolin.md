Category: people
PersonType: researcher
Id: dmitriy_vatolin
Lang: en
Title: Dmitriy S. Vatolin
Date: 2020-01-01
Position: Senior researcher
Email: dmitriy@graphics.cs.msu.ru
Photo: images/dmitriy_vatolin.jpg
ResearchInterests: test1, test2, test3, test4
Projects: project4, project5, project6
SelectedPublications: publ3, publ4

Dmitriy Vatolin graduated from the Applied Mathematics department of Moscow State University in 1996. He defended his Ph.D. thesis on computer graphics in 2000. A co-author of a book on data compression (Russian), published in 2003. A co-founder of compression.ru website — one of the biggest site on data compression and video processing in the world. From 2000 until 2006 took part in 5 start-up computer companies, in 3 as a co-founder, 4 of the companies are operating until now.




Category: publications
id: eval_of_traffic_sign_rec
Date: 2020-01-01
Photo: images/traffic_sign_recognition_small.jpg
Title: Evaluation of Traffic Sign Recognition Methods Trained on Synthetically Generated Data
PaperAuthors: Person. A, anton_konushin, Person. B
Journal: Advanced Concepts for Intelligent Vision Systems (Springer LNCS, Vol. 8192),2011
DownloadLink: https://link.springer.com/chapter/10.1007/978-3-319-02895-8_52
BibTex: @inproceedings{moiseev2013evaluation,
          title={Evaluation of traffic sign recognition methods trained on synthetically generated data},
          author={Moiseev, Boris and Konev, Artem and Chigorin, Alexander and Konushin, Anton},
          booktitle={International Conference on Advanced Concepts for Intelligent Vision Systems},
          pages={576--583},
          year={2013},
          organization={Springer}
        }

Paper abstract content


#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Graphics and Media Lab'
SITENAME = 'Graphics and Media Lab'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Moscow'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

MENUITEMS_LANG = [
    ('ru', [('Главная', '/home-ru.html'),
            ('О лаборатории', '/pages/about-lab-ru.html'),
            ('Сотрудники', '/researchers-ru.html'),
            ('Проекты', '/projects-ru.html'),
            ('Публикации', '/publications-ru.html'),
            ('Загрузки', '/downloads-ru.html'),
            ('Преподавание', '/teaching-ru.html'),
            ('Выпускники', '/alumni-ru.html')]),
    ('en', [('Home', '/home.html'),
            ('About Lab', '/pages/about-lab.html'),
            ('People', '/researchers.html'),
            ('Research', '/projects.html'),
            ('Publications', '/publications.html'),
            ('Downloads', '/downloads.html'),
            ('Teaching', '/teaching.html'),
            ('Alumni', '/alumni.html')]),
]

#DEFAULT_PAGINATION = False

DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False


DIRECT_TEMPLATES = []
PAGINATED_DIRECT_TEMPLATES = []


PAGINATED_TEMPLATES = {'news': 2}

ARTICLE_EXCLUDES = ['page']

CATEGORY_SAVE_AS = ''
AUTHOR_SAVE_AS = ''

ARTICLE_TRANSLATION_ID = ['id']
PAGE_TRANSLATION_ID = ['id']
POSSIBLE_LANGS = ['ru', 'en']

PAGINATION_PATTERNS = (
    (1, '{base_name}.html', '{base_name}.html'),
    (2, '{base_name}/page_{number}.html', '{base_name}/page_{number}.html'),
)

TEMPLATE_PAGES = {
    'home.html': 'index.html',
}

PLUGIN_PATHS = ["."]
PLUGINS = ["my_plugin"]


ARTICLE_URL = '{category}/{slug}.html'
ARTICLE_SAVE_AS = '{category}/{slug}.html'
ARTICLE_LANG_URL = '{category}/{slug}-{lang}.html'
ARTICLE_LANG_SAVE_AS = '{category}/{slug}-{lang}.html'


# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True